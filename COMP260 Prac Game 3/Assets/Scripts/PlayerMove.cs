﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public Vector3 move;
	public float speed = 3f;
	public float stopPos;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if(transform.position.x > stopPos) {
			Vector3 move = speed * Time.deltaTime * Vector3.left;
			transform.Translate (move);
		}
	}
}
