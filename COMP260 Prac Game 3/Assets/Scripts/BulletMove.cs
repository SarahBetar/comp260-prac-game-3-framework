﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	// separate speed and direction so we can 
	// tune the speed without changing the code
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;
	public float bulletExpiryTime = 2.5f;
	private float timer;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();    
	}

	void FixedUpdate () {
		rigidbody.velocity = speed * direction;
	}

	void Update() {
		timer += Time.deltaTime;
		if (timer > bulletExpiryTime) {
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}

}
