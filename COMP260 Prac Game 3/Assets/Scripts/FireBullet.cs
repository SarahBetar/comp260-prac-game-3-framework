﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {
	public BulletMove bulletPrefab;
	public float bulletTimeLimit = 0f;
	public float bulletExpiryTime;
	private float timer;

	// Use this for initialization
	void Start () {
		
	}

	void Update () {
		if (Time.timeScale == 0) {
			return;
		}

		timer += Time.deltaTime;
		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1") && timer > bulletTimeLimit) {

				BulletMove bullet = Instantiate (bulletPrefab);
				// the bullet starts at the player's position
				bullet.transform.position = transform.position;

				// create a ray towards the mouse location
				Ray ray = 
					Camera.main.ScreenPointToRay (Input.mousePosition);
				bullet.direction = ray.direction;
			timer = 0;
		}
	}

}
